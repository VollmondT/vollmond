# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit git-2 autotools eutils
DESCRIPTION="STM32 Stlink utility"
HOMEPAGE="https://github.com/texane/stlink"

V_ECLASS="git-2"

EGIT_REPO_URI="git://github.com/texane/stlink.git"
SRC_URI=""

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# TODO add doc useflag

DEPEND="virtual/libusb:1"
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${PN}-aligment.patch
	eautoreconf
}
