# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-arch/makeself/makeself-2.1.5-r1.ebuild,v 1.7 2012/08/26 18:58:21 armin76 Exp $

EAPI=4


inherit eutils;

DESCRIPTION="shell script that generates a self-extractible tar.gz (fork)"
HOMEPAGE="https://github.com/VollmondT/makeself/"
SRC_URI="https://github.com/downloads/VollmondT/makeself/${P}.run"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 hppa ppc x86 ~amd64-linux ~x86-linux"
IUSE=""

S=${WORKDIR}

DEPEND=">app-arch/unmakeself-1.0"

src_unpack() {
	ebegin unmakeself ${DISTDIR}/$A
	unmakeself ${DISTDIR}/${A} || die
	eend $?
}

src_install() {
	dobin makeself-header.sh makeself.sh || die
	dosym makeself.sh /usr/bin/makeself
	doman makeself.1
	dodoc README.md TODO makeself.lsm
}

